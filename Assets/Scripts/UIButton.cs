using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIButton : MonoBehaviour
{
    // public void OnSelect(BaseEventData eventData)
    // {
    //     Debug.Log(gameObject.name + " selected");
    //     transform.DOScale(new Vector3(1.1f, 1.1f), 0.5f);
    // }

    private void OnMouseEnter()
    {
        Debug.Log(gameObject.name + " selected");
        transform.DOScale(new Vector3(1.1f, 1.1f), 0.5f).SetEase(Ease.Flash);
    }

    private void OnMouseExit()
    {
        Debug.Log(gameObject.name + " unselected");
        transform.DOScale(new Vector3(1, 1), 0.5f);
    }

    public void OnSelect()
    {
        Debug.Log(gameObject.name + " selected");
        transform.DOScale(new Vector3(1.1f, 1.1f), 0.5f).SetEase(Ease.Flash);
    }

    public void OnDeselect()
    {
        Debug.Log(gameObject.name + " unselected");
        transform.DOScale(new Vector3(1, 1), 0.5f).SetEase(Ease.Flash);
    }

    public void OnUse()
    {
        Debug.Log(gameObject.name + " used");
        transform.DOPunchScale(new Vector3(0.7f, 0.7f), 0.5f).SetEase(Ease.Flash);
    }
}
