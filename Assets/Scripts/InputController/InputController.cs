using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

namespace InputController
{
    public class InputDelegates : MonoBehaviourSingleton<InputDelegates>, GameController.IPlayerActions, GameController.IUIActions
    {
        public delegate void OnKeyPressedDelegateFloatArg(float something);
        public delegate void OnKeyPressedDelegateBoolArg(bool something);
        public delegate void OnKeyPressedDelegateNoArgs();

        #region InputDelegates
        public OnKeyPressedDelegateFloatArg OnMoveKeyPressed;
        public OnKeyPressedDelegateBoolArg OnRunKeyPressed;
        public OnKeyPressedDelegateBoolArg OnAttackKeyPressed;
        public OnKeyPressedDelegateBoolArg OnBlockKeyPressed;
        public OnKeyPressedDelegateBoolArg OnKickKeyPressed;
        public OnKeyPressedDelegateNoArgs OnCallKeyPressed;
        public OnKeyPressedDelegateNoArgs OnInteractKeyPressed;
        public OnKeyPressedDelegateNoArgs OnInteractLongKeyPressed;
        public OnKeyPressedDelegateNoArgs OnInteractQuickKeyPressed;
        public delegate void OnKeyPressedDelegate(InputAction.CallbackContext context);
        #endregion
        
        private GameController _gameController;
        private float _afkTimer;
        
        #region MenuDelegates
        public OnKeyPressedDelegateFloatArg OnNavigatePressed;
        public OnKeyPressedDelegateNoArgs OnSelectPressed;
        public OnKeyPressedDelegateNoArgs OnBackPressed;
        public OnKeyPressedDelegateNoArgs OnCancelPressed;
        public OnKeyPressedDelegateNoArgs OnLeftClick;
        #endregion

        protected override void Awake()
        {
            base.Awake();
            _gameController = new GameController();
            _gameController.UI.SetCallbacks(this);
            _gameController.Player.SetCallbacks(this);
            _gameController.Player.Disable();
        }
        
        private void OnEnable()
        {
            _gameController.Enable();
        }

        private void OnDisable()
        {
            _gameController.Disable();
        }

        #region GameInput
        public void OnMove(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (!context.performed) return;

            float direction = context.ReadValue<Vector2>().x;
            if (direction < /*GameConstants.MinControllerDeadZone*/0.15f && direction > -/*GameConstants.MinControllerDeadZone*/0.15f)
            {
                direction = 0;
            }
            
            if (direction > /*GameConstants.MaxControllerDeadZone*/0.85f) 
            {
                direction = 1;
            }

            if (direction < -/*GameConstants.MaxControllerDeadZone*/0.85f)
            {
                direction = -1;
            }
            OnMoveKeyPressed?.Invoke(direction);
        }
        
        public void OnRun(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnRunKeyPressed?.Invoke(context.ReadValue<float>() != 0);
        }

        public void OnAttack(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnAttackKeyPressed?.Invoke(context.ReadValue<float>() != 0);
        }

        public void OnBlock(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnBlockKeyPressed?.Invoke(context.ReadValue<float>() != 0);
        }

        public void OnKick(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnKickKeyPressed?.Invoke(context.ReadValue<float>() != 0);
        }

        private Coroutine _buttonHoldCoroutine;
        public void OnInteract(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.started)
            {
                OnInteractKeyPressed();
                float seconds = 1f;
                _buttonHoldCoroutine ??= StartCoroutine(CheckKeyHeldForTime(seconds));
            }

            if (context.canceled)
            {
                if (null == _buttonHoldCoroutine) return;
                OnInteractQuickKeyPressed();
                
                StopCoroutine(_buttonHoldCoroutine);
                _buttonHoldCoroutine = null;
                
            }
        }
        
        public void OnCall(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnCallKeyPressed();
        }
        
        private IEnumerator CheckKeyHeldForTime(float seconds)
        {
            var waitSeconds = new WaitForSeconds(seconds);
            yield return waitSeconds;
            
            OnInteractLongKeyPressed();
            _buttonHoldCoroutine = null;
        }
        
        #endregion

        public bool IsAfk()
        {
            return Time.time - _afkTimer > 20f/*GameSettings.Instance.gameSettingsScriptableObject.afkThreshold*/;
        }

        public void SwitchToMenu()
        {
            _gameController.Player.Disable();
            _gameController.UI.Enable();
            Debug.Log("Switched Input to Menu");
        }
        
        public void SwitchToGame()
        {
            _gameController.UI.Disable();
            _gameController.Player.Enable();
            Debug.Log("Switched Input to Game");
        }
        
        #region MenuInput
        public void OnNavigate(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (!context.performed) return;
            
            float direction = context.ReadValue<Vector2>().y;
            if (direction > 0.2f)
            {
                direction = 1;
            } else if (direction < -0.2f)
            {
                direction = -1;
            }
            OnNavigatePressed?.Invoke(direction);
        }

        public void OnSelect(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnSelectPressed?.Invoke();
        }

        public void OnBack(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnBackPressed?.Invoke();
        }

        public void OnCancel(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnCancelPressed?.Invoke();
        }

        public void OnClick(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                OnLeftClick?.Invoke();
        }
        #endregion
    }
}
