namespace Utils.Update
{
    public interface IUpdateable
    {
        void MyUpdate();
        bool ShouldUpdate();
    }
}