using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Utils.Update;

namespace Utils
{
    public class ButtonHoldAnimation : MonoBehaviour
    {
        public delegate void OnActionCompleted();
        public OnActionCompleted actionCompleted;
        private Tween _tween;
        private Tween _successTween;
        private Tween _failTween;
        private SpriteRenderer _spriteRenderer;
        
        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            gameObject.transform.localScale = new Vector3(0, 0);
            _spriteRenderer.color = new Color(55f / 255f, 61f / 255f,49f / 255f, 1);
        }

        public void Play(float seconds = 1)
        {
            _spriteRenderer.color = new Color(55f / 255f, 61f / 255f,49f / 255f, 1); // neutral color
            _tween = transform.DOScale(new Vector3(1, 1), seconds);
            _tween.OnComplete(OnComplete);
        }
        
        public void Stop()
        {
            _tween.Kill();
            _failTween = transform.DOShakeScale(0.5f).OnComplete(() =>
            {
                gameObject.transform.localScale = new Vector3(0, 0);
                Debug.Log("ButtonHold Fail Tween Done");
            });
            Debug.Log("ButtonHold Fail");
        }
        
        private void OnComplete()
        {
            actionCompleted?.Invoke();
            
            _successTween = transform.DOScale(new Vector3(0, 0), 0.5f).OnComplete(() =>
            {
                Debug.Log("ButtonHold Tween Done"); 
            });
            Debug.Log("ButtonHold OnComplete");
        }
        
#if UNITY_EDITOR
        
        private void TestSuccess()
        {
            _tween = transform.DOScale(new Vector3(1, 1), 2).OnComplete(() =>
            {
                _spriteRenderer.color = new Color(20f / 255f, 91f / 255f,0, 1);// success color
                _successTween = transform.DOScale(new Vector3(0, 0), 0.5f).OnComplete(() =>
                {
                    Debug.Log("ButtonHold Tween Done"); 
                });
                Debug.Log("ButtonHold OnComplete");
            });
        }

        private IEnumerator TestKill()
        {
            _tween = transform.DOScale(new Vector3(1, 1), 2);
            yield return new WaitForSeconds(0.5f);
            _spriteRenderer.color = new Color(181f / 255f, 0,2f / 255f, 1);// fail color
            _tween.Kill();
            _failTween = transform.DOPunchScale(new Vector3(1.2f,0.5f),0.5f).OnComplete(() =>
            {
                gameObject.transform.localScale = new Vector3(0, 0);
                Debug.Log("ButtonHold Fail Tween Done");
            });
            Debug.Log("ButtonHold Fail");
        }
#endif
    }
}
