using System;
using System.Collections.Generic;
using DG.Tweening;
using InputController;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace MenuAndStartup
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private GameObject[] buttons;
        private List<UIButton> buttonsScript;
        [SerializeField] private int nbOfButtons;
        private int m_Index = 0;

        private void Start()
        {
            nbOfButtons = buttons.Length - 1;
            buttonsScript = new List<UIButton>(nbOfButtons);
            foreach (var button in buttons)
            {
                var tmp = button.GetComponent<UIButton>();
                if (null == tmp)
                {
                    Debug.LogError("why does button not have UIButton script");
                }
                buttonsScript.Add(tmp);
            }
            
            buttonsScript[m_Index].OnSelect();
        }

        public void PlayButton()
        {
            InputDelegates.Instance.SwitchToGame();
            SceneManager.LoadScene($"Game");
        }
        
        public void QuitButton()
        {
            Application.Quit();
        }

        private void OnEnable()
        {
            InputDelegates.Instance.OnNavigatePressed += OnNavigate;
            InputDelegates.Instance.OnSelectPressed += OnSelect;
            InputDelegates.Instance.OnBackPressed += OnBack;
        }

        private void OnDisable()
        {
            InputDelegates.Instance.OnNavigatePressed -= OnNavigate;
            InputDelegates.Instance.OnSelectPressed -= OnSelect;
            InputDelegates.Instance.OnBackPressed -= OnBack;
        }

        private void OnSelect()
        {
            buttonsScript[m_Index].OnUse();
            switch (m_Index)
            {
                case 0:
                    Debug.Log("You pressed Play");
                    // PlayButton();
                    break;
                case 1:
                    // QuitButton();
                    Debug.Log("You pressed Quit");
                    break;
            }
        }
        
        private void OnBack()
        {
            Debug.Log("You pressed ESC");
        }

        private void OnNavigate(float dir)
        {
            Debug.Log("dir = " + dir + " index = " + m_Index);
            DeselectButton(m_Index);
            // up
            if (dir > 0)
            {
                if (m_Index == 0) return;
                
                --m_Index;
            }
            else // down
            {
                if (m_Index >= nbOfButtons) return;

                ++m_Index;
            }

            SelectButton(m_Index);
            Debug.Log("Index after = " + m_Index);
        }

        private void SelectButton(int index)
        {
            buttonsScript[index].OnSelect();
        }
        
        private void DeselectButton(int index)
        {
            buttonsScript[index].OnDeselect();
        }
    }
}
