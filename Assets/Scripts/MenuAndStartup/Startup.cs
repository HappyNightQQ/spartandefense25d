using System;
using System.Collections;
using InputController;
using UnityEngine;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;

namespace MenuAndStartup
{
    public class Startup : MonoBehaviour
    {
        private AudioSource _startupSound;

        private void Start()
        {
            gameObject.AddComponent<InputSystemUIInputModule>();
            _startupSound = GetComponent<AudioSource>();
            _startupSound.Play();
            DontDestroyOnLoad(_startupSound);

            StartCoroutine(Wait(21));
        }

        private void OnEnable()
        {
            InputDelegates.Instance.OnAttackKeyPressed += GoToMainMenu;
            InputDelegates.Instance.OnBlockKeyPressed += GoToMainMenu;
            InputDelegates.Instance.OnKickKeyPressed += GoToMainMenu;
            InputDelegates.Instance.OnRunKeyPressed += GoToMainMenu;
            InputDelegates.Instance.SwitchToMenu();
        }
        
        private void OnDisable()
        {
            InputDelegates.Instance.OnAttackKeyPressed -= GoToMainMenu;
            InputDelegates.Instance.OnBlockKeyPressed -= GoToMainMenu;
            InputDelegates.Instance.OnKickKeyPressed -= GoToMainMenu;
            InputDelegates.Instance.OnRunKeyPressed -= GoToMainMenu;
        }

        private void GoToMainMenu(bool something = false)
        {
            SceneManager.LoadScene($"MainMenu");
        }

        private IEnumerator Wait(float sec)
        {
            var waitSeconds = new WaitForSeconds(sec);
            yield return waitSeconds;

            GoToMainMenu(false);
        }
    }
}
